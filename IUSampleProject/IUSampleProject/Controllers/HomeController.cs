using Microsoft.AspNetCore.Mvc;

namespace IUSampleProject.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class HomeController : ControllerBase
    {
        [HttpGet]
        public string GetHelloWorld()
        {
            return "Hello World!";

        }

    }
}