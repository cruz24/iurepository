terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "2.90.0"
    }
  }
}

provider "azurerm" {
  features {}
  subscription_id = "c294850d-b6e7-43f5-a3fe-71c57ee8fe23"
}

resource "azurerm_resource_group" "iuresourcegroup" {
  name     = "infra-resourcegroup"
  location = "Australia East"
}

resource "azurerm_kubernetes_cluster" "iuresourcegroupaks" {
  name                = "iuresourcegroup-aks1"
  location            = azurerm_resource_group.iuresourcegroup.location
  resource_group_name = azurerm_resource_group.iuresourcegroup.name
  dns_prefix          = "iuresourcegroupaks1"

  default_node_pool {
    name       = "default"
    node_count = 1
    vm_size    = "Standard_D2_v2"
  }

  identity {
    type = "SystemAssigned"
  }

  tags = {
    Environment = "Development"
  }
  
}

# Azure Container Registry
resource "azurerm_container_registry" "iuresourcegroupacr" {
  name                = "iuresourcegroupacr1"
  resource_group_name = azurerm_resource_group.iuresourcegroup.name
  location            = azurerm_resource_group.iuresourcegroup.location
  sku                 = "Basic"
  admin_enabled       = true
  tags = {
    Environment = "Development"
  }
}

# Attached Azure Container Registry to ubergroceryK8s
# resource "azurerm_role_assignment" "iuresourcegroupK8sAcr" {
#   scope                = azurerm_container_registry.iuresourcegroupacr.id
#   role_definition_name = "AcrPull"
#   principal_id         = azurerm_kubernetes_cluster.iuresourcegroupaks.kubelet_identity[0].object_id
# }


# az aks update -n iuresourcegroup-aks1 -g infra-resourcegroup --attach-acr iuresourcegroupacr1